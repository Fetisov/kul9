function check(){
	switch(form.startingScaleOfNotation.value){
		case '2': 
			regExr(/[^01.-]/);
			break;	 
		case '8':
			regExr(/[^0-7.-]/);
			break;
		case '10':
			regExr(/[^0-9.-]/);
			break;
		case '16':
			regExr(/[^0-9A-Fa-f.-]/);
			break;
			
	}
	
}
function regExr(reg){
	if(reg.test(form.number.value) || !form.numberToConvert.value  || form.numberToConvert.value == "." || form.numberToConvert.value == "-"){
		form.result.value ="Ошибка ввода данных";
	}
	else{
		form.result.value = "";
		convert();
	}
}
function convert(){
	
	var parentElem = document.body.children[0];
	
	var startingNumber = document.createElement('output');
	var startingScaleOfNotation = document.createElement('sub');
	var equal = document.createElement('output');
	var result = document.createElement('output');
	var finalScaleOfNotation = document.createElement('sub');
	
	
	startingNumber.innerHTML = form.numberToConvert.value;
	startingScaleOfNotation.innerHTML = form.startingScaleOfNotation.value + " с/c";
	equal.innerHTML = " = ";
	parentElem.appendChild(startingNumber);
	parentElem.appendChild(startingScaleOfNotation);
	parentElem.appendChild(equal);
	
	result.innerHTML = parseInt(form.number.value,form.startingScaleOfNotation.value).toString(form.finalScaleOfNotation.value);
	finalScaleOfNotation.innerHTML = form.finalScaleOfNotation.value + " с/с";
	parentElem.appendChild(result);
	parentElem.appendChild(finalScaleOfNotation);
}
function resetFields(){
	location.reload()
	document.form.reset();
}